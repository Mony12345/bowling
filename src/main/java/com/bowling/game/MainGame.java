package com.bowling.game;

import java.util.InputMismatchException;
import java.util.Scanner;

public class MainGame {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Game game = new Game();
        try {
            System.out.println("New Bowling game strarted");
            System.out.println("-------------------------");
            System.out.println(" ");
            System.out.println("Insert number of pins down:");

            while (true) {
                try {
                    game.roll(scan.nextInt());
                    if (game.gameIsEnd) {
                        System.out.println("Game end with the score of " + game.score());
                        break;
                    } else {
                        System.out.println("Insert number of pins down:");
                    }
                } catch(InputMismatchException e){
                    System.out.println("ERROR: input expected is a number");
                    break;
                } catch (Exception e1) {
                    System.out.println(e1.getMessage());
                    System.out.println("Insert the correct number of pins down:");
                }
            }
        } catch (Exception e) {
            System.out.println("ERROR: " + e.getMessage());
        } finally {
            scan.close();
        }
    }
}

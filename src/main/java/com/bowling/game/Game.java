package com.bowling.game;

import com.bowling.frame.Frame;
import com.bowling.frame.Roll;

public class Game {
    private final Frame[] frames;
    private Frame actualFrame;
    private int actualNumberFrame;
    public boolean gameIsEnd;

    public Game() {
        this.frames = new Frame[10];
        this.actualFrame = new Frame();
        this.actualNumberFrame = 0;
        gameIsEnd = false;
    }

    public void roll(int pins) throws Exception {
        if (actualNumberFrame < 9) {
            rollNormalFrame(pins);
        } else if (actualNumberFrame == 9) {
            rollLastFrame(pins);
        } else {
            throw new Exception("Game already over with the score of " + score());
        }
    }

    private void rollNormalFrame(int pins) throws Exception {
        if (actualFrame.getFrameSize() == 1) {
            if (actualFrame.getRoll(0).getPins() + pins > Roll.MAX_PINS) {
                throw new Exception("Number of pins not valid");
            }
        }
        actualFrame.addRolls(new Roll(pins));
        if (actualFrame.isStrike() || actualFrame.getFrameSize() == 2) {
            frames[actualNumberFrame] = actualFrame;
            actualNumberFrame++;
            actualFrame = new Frame();
        }
    }

    private void rollLastFrame(int pins) throws Exception {
        Roll r = new Roll(pins);
        switch (actualFrame.getFrameSize()) {
            case 0:
                actualFrame.addRolls(r);
                break;
            case 1:
                actualFrame.addRolls(r);
                if (!actualFrame.isSpare()) {
                    if (!r.isStrike()) {
                        if (!actualFrame.getRoll(0).isStrike()) {
                            frames[actualNumberFrame] = actualFrame;
                            actualNumberFrame++;
                            actualFrame = new Frame();
                            gameIsEnd = true;
                        }
                    }
                }
                break;
            case 2:
                actualFrame.addRolls(r);
                frames[actualNumberFrame] = actualFrame;
                actualNumberFrame++;
                actualFrame = new Frame();
                gameIsEnd = true;
                break;
        }
    }

    public int score() {
        int score = 0;
        for (int i = 0; i < 9; i++) {
            score += frames[i].getRollsScore();
            if (frames[i].isSpare()) {
                score += frames[i + 1].getRoll(0).getPins();
            }
            if (frames[i].isStrike()) {
                if (frames[i + 1].isStrike()) {
                    if (i == 7) {
                        if (frames[9].isStrike()) {
                            score += Roll.MAX_PINS * 2;
                        } else {
                            score += frames[8].getRollsScore() + frames[9].getRoll(0).getPins() + frames[9].getRoll(1).getPins();
                        }

                    } else if (i == 8) {
                        if (frames[9].getRoll(1).isStrike()) {
                            score += Roll.MAX_PINS * 2;
                        } else {
                            score += frames[9].getRollsScore();
                        }
                    } else {
                        score += frames[i + 1].getRollsScore() + frames[i + 2].getRollsScore();
                    }
                } else {
                    if (i == 8) {
                        score += frames[i + 1].getRoll(0).getPins() + frames[i + 1].getRoll(1).getPins();
                    } else {
                        score += frames[i + 1].getRollsScore();
                    }
                }
            }
        }
        score += frames[9].getRollsScore();
        return score;
    }
}

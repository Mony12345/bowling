package com.bowling.frame;

import java.util.ArrayList;
import java.util.List;

public class Frame {

	private List<Roll> rolls;

	public Frame() {
		rolls = new ArrayList<>();
	}

	public void addRolls(Roll r) {
		rolls.add(r);
	}

	public Roll getRoll(int i) {
		return rolls.get(i);
	}

	public boolean isStrike() {
		return rolls.get(0).isStrike();
	}

	public boolean isSpare() {
		if (rolls.size() >= 2) {
			if (rolls.get(0).getPins() + rolls.get(1).getPins() == Roll.MAX_PINS)
				return true;
		}
		return false;
	}

	public int getRollsScore() {
		int score = 0;
		for (Roll r : rolls) {
			score = score + r.getPins();
		}
		return score;
	}

	public int getFrameSize() {
		return rolls.size();
	}

	@Override
	public String toString() {
		return "Frame{" +
				"rolls=" + rolls +
				'}';
	}
}

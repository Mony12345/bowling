package com.bowling.frame;

public class Roll {
	public static final int MAX_PINS = 10;
	private int pins;

	public Roll(int pins) throws Exception {
		if (pins <= MAX_PINS) {
			this.pins = pins;
		} else {
			throw new Exception("Wrong number of pin, max pin is " + String.valueOf(MAX_PINS));
		}
	}

	public boolean isStrike() {
		return pins == MAX_PINS;
	}

	public int getPins() {
		return pins;
	}

	@Override
	public String toString() {
		return "Roll{" +
				"pins=" + pins +
				'}';
	}
}

package com.bowling.game;

import org.junit.jupiter.api.Test;

import com.bowling.game.Game;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestGame {
	private Game game;

	@Test
	public void testNoSpareAndNoStrike() throws Exception {
		game = new Game();
		var rolls = new int[]{1,2,3,4,5,1,2,3,4,5,1,2,3,4,5,1,2,3,4,5};
		for (int roll : rolls) {
			game.roll(roll);
		}
		assertEquals(60, game.score());
	}

	@Test
	public void testGameEnd(){
		game = new Game();
		try {
			var rolls = new int[]{1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 1, 2, 3, 4, 5};
			for (int roll : rolls) {
				game.roll(roll);
			}
		}catch (Exception e){
			assertTrue(e.getMessage().contains("Game already over with the score of "));
		}
	}

	@Test
	public void testOneSpareInFirstFrame() throws Exception {
		game = new Game();
		var rolls = new int[]{5,5,3,4,5,1,2,3,4,5,1,2,3,4,5,1,2,3,4,5};
		for (int roll : rolls) {
			game.roll(roll);
		}
		assertEquals(70, game.score());
	}

	@Test
	public void testOneSpareInLastFrame() throws Exception {
		game = new Game();
		var rolls = new int[]{1,2,3,4,5,1,2,3,4,5,1,2,3,4,5,1,2,3,6,4,5};
		for (int roll : rolls) {
			game.roll(roll);
		}
		assertEquals(66, game.score());
	}

	@Test
	public void testOneStrikeInFirstFrame() throws Exception {
		game = new Game();
		var rolls = new int[]{10,3,4,5,1,2,3,4,5,1,2,3,4,5,1,2,3,4,5};
		for (int roll : rolls) {
			game.roll(roll);
		}
		assertEquals(74, game.score());
	}

	@Test
	public void testOneStrikeInLastFrame() throws Exception {
		game = new Game();
		var rolls = new int[]{1,2,3,4,5,1,2,3,4,5,1,2,3,4,5,1,2,3,10,5,3};
		for (int roll : rolls) {
			game.roll(roll);
		}
		assertEquals(69, game.score());
	}

	@Test
	public void testAllStrikes() throws Exception {
		game = new Game();
		var rolls = new int[]{10,10,10,10,10,10,10,10,10,10,10,10};
		for (int roll : rolls) {
			game.roll(roll);
		}
		assertEquals(300, game.score());
	}

	@Test
	public void testAllSpares() throws Exception {
		game = new Game();
		var rolls = new int[]{5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5};
		for (int roll : rolls) {
			game.roll(roll);
		}
		assertEquals(150, game.score());
	}

	@Test
	public void testNoPinsDownEachRound() throws Exception {
		game = new Game();
		var rolls = new int[]{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
		for (int roll : rolls) {
			game.roll(roll);
		}
		assertEquals(0, game.score());
	}

	@Test
	public void testExerciseCase() throws Exception {
		game = new Game();
		var rolls = new int[]{1,4,4,5,6,4,5,5,10,0,1,7,3,6,4,10,2,8,6};
		for (int roll : rolls) {
			game.roll(roll);
		}
		assertEquals(133, game.score());
	}
}

package com.bowling.frame;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class TestFrame {
    Frame frame;

    @Test
    void TestIsSpareCorrect() throws Exception {
        frame = new Frame();
        frame.addRolls(new Roll(3));
        frame.addRolls(new Roll(7));
        assertTrue(frame.isSpare());

    }

    @Test
    void TestIsSpareWrong() throws Exception {
        frame = new Frame();
        frame.addRolls(new Roll(1));
        frame.addRolls(new Roll(1));
        assertFalse(frame.isSpare());
    }


    @Test
    void getRollsScore() throws Exception {
        frame = new Frame();
        frame.addRolls(new Roll(1));
        frame.addRolls(new Roll(1));
        assertEquals(2, frame.getRollsScore());
    }

    @Test
    void getRollsScoreStrikeLastFrame() throws Exception {
        frame = new Frame();
        frame.addRolls(new Roll(10));
        frame.addRolls(new Roll(10));
        frame.addRolls(new Roll(10));
        assertEquals(30, frame.getRollsScore());
    }

}
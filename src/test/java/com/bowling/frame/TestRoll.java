package com.bowling.frame;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TestRoll {
    private Roll roll;

    @Test
    void testCorrectPin() {
        try {
            roll = new Roll(2);
        } catch (Exception e) {
            fail("Should not have thrown any exception");
        }
    }

    @Test
    void testWrongPin() {
        Exception exception = assertThrows(Exception.class, () -> {
            roll = new Roll(20);
        });
        assertTrue(exception.getMessage().contains("Wrong number of pin, max pin is"));
    }

    @Test
    void testStrikeCorrect() throws Exception{
        roll = new Roll(Roll.MAX_PINS);
        assertTrue(roll.isStrike());
    }

    @Test
    void testStrikeWrong() throws Exception{
        roll = new Roll(2);
        assertFalse(roll.isStrike());
    }
}
